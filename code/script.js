//on récupère la liste des container
const container = document.querySelectorAll('.rule-container');

if ("ontouchstart" in document.documentElement) { //On vérifie si on est sur un touch device
    container.forEach((div) => {
        //on récupère les élément caché 
        const rule = div.querySelector('.rule');
        const ruleImage = div.querySelector('.rule-image');
        const title = div.querySelector('.rule-title');
        //on les fait apparaître 
        title.classList.add('title-show');
        rule.classList.add('rule-show');
        ruleImage.classList.add('rule-image-show');
    })
}
else {
    window.addEventListener('scroll', () => {
        container.forEach((div) => {
            // Quand la valeur du scroll horizontale est = 
            // à la moitié de la position d'un container on l'affiche
            const rect = div.getBoundingClientRect();
            const mid = rect.bottom / 2 + rect.top;
            if (scrollY > mid) {
                //on récupère les élément caché 
                const rule = div.querySelector('.rule');
                const ruleImage = div.querySelector('.rule-image');
                const title = div.querySelector('.rule-title');
                //on les fait apparaître 
                title.classList.add('title-show');
                rule.classList.add('rule-show');
                ruleImage.classList.add('rule-image-show');
            }
        })
    })
}

