//On récupère les boutons et la div que l'on va modifier
const details = document.querySelectorAll('.bouton')
const content = document.getElementById('content')
var onFocus = false;

/*
Quand on clique sur "Détails" on déclanche l'appel à la fonction
affichedetails() qui passe en paramètre la div que le veut modifier
ainsi que son id pour savoir sur quel forfaits l'utilisateur à cliqué.
*/
details[0].onclick = () => {
  // var content = document.getElementById('content')
  affichedetails(content, details[0].id)
  onFocus = true;
}
details[1].onclick = () => {
  // var content = document.getElementById('content')
  affichedetails(content, details[1].id)
  onFocus = true;
}
details[2].onclick = () => {
  // var content = document.getElementById('content')
  affichedetails(content, details[2].id)
  onFocus = true;
}

function affichedetails(content, nb) {
  //console.log('details')
  if (nb === 'b1') {
    content.innerHTML =
      `
    <div class="container-focus">
    <h3 id="close" class="close">RETOUR</h3>
    <img src="Image/Apprenti.png"> 
    <div class="title">
      <p> PERMIS B</p>
    </div>
    <div class="focus-first">
      <div class="focus-head">
        <h2>Phase théorique</h2>
      </div>
      <div class="content">
        <p>Frais d'inscription</p>
        <p>Frais d'enregistrement (ANTS)</p>
        <p>Livre de code</p>
        <p>Test en salle (6 mois)</p>
        <p>Accès internet</p>
        <h3 class="prix">285 €</h3>
      </div>
    </div> 
    <h3 class="plus">+</h3>
    <div class="focus-second">
      <div class=focus-head>
        <h2>Phase pratique</h2>
      </div>
      <div class="content">
        <p>Fiche de suivi</p>
        <p>20H de conduite<p>
        <p>-</p>
        <p>-</p>
        <p>-</p>
        <h3 class="prix">5 x 201 € *</h3>
      </div>
    </div>
    <div class="asterix">
      <h4>* Encaissements à la 1ère, 5ème, 10ème, 15ème et 20ème heure de conduite.</h4>
      <a href="PDF/détails PERMIS B.pdf" target="_blank">Télécharger le détails</a>
    </div>
  </div>

  <div class="container">
  <div class="item">
    <p>PERMIS AAC</p>
    <div class="img">
      <img src="Image/conduite-accompagnee.png">
    </div>
    <a id="b2" class="bouton">Détails</a>
  </div>
  <div class="item">
    <p>PERMIS CS</p>
    <div class="img">
      <img src="Image/permis-supervisee.png">
    </div>
    <a id="b3" class="bouton">Détails</a>
  </div>
    `
  }
  else if (nb === 'b2') {
    content.innerHTML =
      `
    <div class="container-focus">
    <h3 id="close" class="close">RETOUR</h3>
    <img src="Image/conduite-accompagnee.png"> 
    <div class="title">
      <p> PERMIS AAC</p>
    </div>
    <div class="focus-first">
      <div class="focus-head">
        <h2>Phase théorique</h2>
      </div>
      <div class="content">
        <p>Frais d'inscription</p>
        <p>Frais d'enregistrement (ANTS)</p>
        <p>Livre de code</p>
        <p>Test en salle (6 mois)</p>
        <p>Accès internet</p>
        <h3 class="prix">285 €</h3>
      </div>
    </div> 
    <h3 class="plus">+</h3>
    <div class="focus-second">
      <div class=focus-head>
        <h2>Phase pratique</h2>
      </div>
      <div class="content">
        <p>Fiche de suivi</p>
        <p>20H de conduite<p>
        <p>-</p>
        <p>-</p>
        <p>-</p>
        <h3 class="prix">5 x 223 € *</h3>
      </div>
    </div>
    <div class="asterix">
      <h4>* Encaissements à la 1ère, 5ème, 10ème, 15ème et 20ème heure de conduite.</h4>
      <a href="PDF/détails AAC.pdf" target="_blank">Télécharger le détails</a>
    </div>
  </div>

  <div class="container">
  <div class="item">
        <p>PERMIS B</p>
        <div class="img">
          <img src="Image/Apprenti.png">
        </div>
        <a id="b1" class="bouton">Détails</a>
      </div>
  <div class="item">
    <p>PERMIS CS</p>
    <div class="img">
      <img src="Image/permis-supervisee.png">
    </div>
    <a id="b3" class="bouton">Détails</a>
  </div>
    `
  }
  else {
    content.innerHTML =
      `
    <div class="container-focus">
    <h3 id="close" class="close">RETOUR</h3>
    <img src="Image/permis-supervisee.png"> 
    <div class="title">
      <p> PERMIS CS</p>
    </div>
    <div class="focus-first">
      <div class="focus-head">
        <h2>Phase théorique</h2>
      </div>
      <div class="content">
        <p>Frais d'inscription</p>
        <p>Frais d'enregistrement (ANTS)</p>
        <p>Livre de code</p>
        <p>Test en salle (6 mois)</p>
        <p>Accès internet</p>
        <h3 class="prix">285 €</h3>
      </div>
    </div> 
    <h3 class="plus">+</h3>
    <div class="focus-second">
      <div class=focus-head>
        <h2>Phase pratique</h2>
      </div>
      <div class="content">
        <p>Fiche de suivi</p>
        <p>20H de conduite<p>
        <p>1 rendez-vous préalable de 2H avec les accompagnants</p>
        <p>-</p>
        <p>-</p>
        <h3 class="prix">5 x 223 € *</h3>
      </div>
    </div>
    <div class="asterix">
      <h4>* Encaissements à la 1ère, 5ème, 10ème, 15ème et 20ème heure de conduite.</h4>
      <a href="PDF/détailsCS.pdf" target="_blank">Télécharger le détails</a>
    </div>
  </div>

  <div class="container">
      <div class="item">
        <p>PERMIS B</p>
        <div class="img">
          <img src="Image/Apprenti.png">
        </div>
        <a id="b1" class="bouton">Détails</a>
      </div>
      <div class="item">
        <p>PERMIS AAC</p>
        <div class="img">
          <img src="Image/conduite-accompagnee.png">
        </div>
        <a id="b2" class="bouton">Détails</a>
      </div>
    `
  }


  const close = document.getElementById('close');
  if (window.innerWidth < 600) {
    close.innerHTML = 'X';
  }
  else {
    close.innerHTML = 'RETOUR';
  }

  var fermer = document.getElementById('close')
  fermer.onclick = () => {
    afficheforfaits(content)
    onFocus = false;
  }

  const details = document.querySelectorAll('.bouton')
  details[0].onclick = () => {
    // var content = document.getElementById('content')
    affichedetails(content, details[0].id)
    // console.log(details[0].id)
    onFocus = true;
  }
  details[1].onclick = () => {
    // var content = document.getElementById('content')
    affichedetails(content, details[1].id)
    // console.log(details[1].id)
    onFocus = true;
  }
}

function afficheforfaits(content) {
  // console.log('close')
  content.innerHTML =
    `<div class="container">
    <div class="item">
      <p>PERMIS B</p>
      <div class="img">
        <img src="Image/Apprenti.png">
      </div>
      <a id="b1" class="bouton">Détails</a>
    </div>
    <div class="item">
      <p>PERMIS AAC</p>
      <div class="img">
        <img src="Image/conduite-accompagnee.png">
      </div>
      <a id="b2" class="bouton">Détails</a>
    </div>
    <div class="item">
      <p>PERMIS CS</p>
      <div class="img">
        <img src="Image/permis-supervisee.png">
      </div>
      <a id="b3" class="bouton">Détails</a>
    </div>
  </div>`

  const details = document.querySelectorAll('.bouton')
  details[0].onclick = () => {
    // var content = document.getElementById('content')
    affichedetails(content, details[0].id)
    // console.log(details[0].id)
    onFocus = true;
  }
  details[1].onclick = () => {
    // var content = document.getElementById('content')
    affichedetails(content, details[1].id)
    // console.log(details[1].id)
    onFocus = true;
  }
  details[2].onclick = () => {
    // var content = document.getElementById('content')
    affichedetails(content, details[2].id)
    // console.log(details[2].id)
    onFocus = true;
  }
}


